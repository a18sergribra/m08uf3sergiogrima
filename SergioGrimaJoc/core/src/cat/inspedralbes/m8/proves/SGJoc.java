package cat.inspedralbes.m8.proves;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Random;

public class SGJoc extends ApplicationAdapter {	
	SpriteBatch batch;
	
	//Model de dades
	Rectangle player;
	List<Rectangle> enemics;
	List<Rectangle> disparsJugador;
	
	//Gr�fics
	Texture playerTexture;
	Texture enemicTexture;
	Texture disparTexture;
	float tempsUltimEnemic=0;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		
		player = new Rectangle(0, 0, Constants.AMPLE_PLAYER, Constants.ALT_PLAYER);
		enemics = new ArrayList<Rectangle>();
		disparsJugador = new ArrayList<Rectangle>();
		
		playerTexture = new Texture(Gdx.files.internal("naus/player.png"));
		enemicTexture = new Texture(Gdx.files.internal("naus/enemy.png"));
		disparTexture = new Texture(Gdx.files.internal("lasers/laserBlue.png"));
		
		//Activem el sistema de Log amb el nivell DEBUG
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//1. Gestio input
		gestionarInput();
		
		//2. Calculs enemics
		actualitza();
		
		//3. Dibuixat
		batch.begin();
		batch.draw(playerTexture, player.x, player.y, player.width, player.height);
		
		for(Rectangle enemic : enemics) {
			batch.draw(enemicTexture, enemic.x, enemic.y, enemic.width, enemic.height);
		}
		
		for(Rectangle dispar : disparsJugador) {
			batch.draw(disparTexture, dispar.x, dispar.y, dispar.width, dispar.height);
		}
		batch.end();
	}
	
	private void actualitza() {
		float delta = Gdx.graphics.getDeltaTime();
		
		tempsUltimEnemic += delta;
		if(tempsUltimEnemic > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int x = new Random().nextInt(480);
			int y=800;
			
			Rectangle nouEnemic = new Rectangle(x, y, Constants.AMPLE_ENEMIC, Constants.ALT_ENEMIC);
			enemics.add(nouEnemic);
			
			tempsUltimEnemic=0;
		}
		
		for(Rectangle dispar : disparsJugador) {
			dispar.y += Constants.VELOCITAT_DISPAR_PLAYER*delta;
		}
		
		for(Rectangle enemic : enemics) {
			enemic.y -= Constants.VELOCITAT_NAU_ENEMIC*delta;
		}
		
		for(Rectangle enemic: enemics) {
			if(enemic.overlaps(player)) {
				Gdx.app.exit();
			}
		}
		
		for(Iterator iterEnemics = enemics.iterator(); iterEnemics.hasNext();) {
			Rectangle enemic = (Rectangle) iterEnemics.next();
			
			for(Iterator iterDispar = disparsJugador.iterator(); iterDispar.hasNext();) {
				Rectangle dispar = (Rectangle) iterDispar.next();
				if(enemic.overlaps(dispar)) {
					iterDispar.remove();
					iterEnemics.remove();
					break;
				}
			}
		}
		
		//Exemple de Depuraci� - Mostro info de enemics
		Gdx.app.debug("Enemics", "Hi ha " + enemics.size() + " enemics");
		
		for(Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			Rectangle enemic = (Rectangle) iterator.next();
			
			if(enemic.y < -enemic.height) {
				iterator.remove();
				//Depuraci� - Comprovo que s'elimina enemic
				Gdx.app.debug("Enemics", "Elimino");
			}
		}
		
		//Depuraci� - Comprovo que hi ha un enemic menys
		Gdx.app.debug("Enemics", "Hi ha " + enemics.size() + " enemics");
	}

	private void gestionarInput() {
		float delta = Gdx.graphics.getDeltaTime();
		
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.x -= Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.x += Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			player.y += Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		
		if(Gdx.input.isKeyPressed(Keys.DOWN)) { 
			player.y -= Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			float x = player.x + player.width / 2;
			float y = player.y + player.height;
			disparsJugador.add(new Rectangle(x, y, Constants.AMPLE_LASER, Constants.ALT_LASER));
		}
			
		int amplePantalla=Gdx.graphics.getWidth();
		int altPantalla = Gdx.graphics.getHeight();
		
		if(player.x < 0) {
			player.x = 0;
		}
		
		if(player.x > (amplePantalla-player.width)) {
			player.x = (amplePantalla-player.width);
		}
		
		if(player.y < 0) {
			player.y = 0;
		}
		
		if(player.y > altPantalla - player.height) {
			player.y = altPantalla - player.height;
		}
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		playerTexture.dispose();
		enemicTexture.dispose();
	}
}
